'''
This module stores utily methods to handle file I/overwritten'''

def store_content(content, path):
    '''
    Stores the provided content into the destination path.
    In case the file already exists, it will be overwritten.
    '''
    dest_file = open(path, 'w+')
    dest_file.seek(0)
    dest_file.write(content)
    dest_file.truncate()
    dest_file.close()
